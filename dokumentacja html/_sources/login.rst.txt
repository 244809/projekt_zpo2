Log in
======================

First login
--------------------
Remember to verify your email address first. You won't be able to log in without it.

.. image:: gifs/ver.gif
           :width: 300
           :align: center

Correct login
--------------------
To log in enter the data with which you registered.

.. image:: gifs/log_complete.gif
           :width: 300
           :align: center

Incorrect login
--------------------
If you enter an incorrect email or password, login will be failed.

.. image:: gifs/log_failed.gif
           :width: 300
           :align: center

Restore password
--------------------

If you have forgotten your password or want to change it, enter your email address in the login screen and touch the password recovery button.
You will receive an e-mail that will allow it.

.. image:: gifs/for.gif
           :width: 300
           :align: center

.. image:: images/for1.JPG
.. image:: images/for2.JPG
           :width: 300
           :align: center
