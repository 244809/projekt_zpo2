Blood glucose measurement
===========================

The user can go to this tab via the side menu or from the home view by clicking the button with the "+" icon below the graph.
The user can add blood glucose results to his database.

Measurement and reasults
--------------------------
After selecting the measurement type, entering its result and pressing the SUBMIT button, the result will be automatically saved to the database and it will be visible on the graph in the home view.
After pressing the REASULT button, the user will be taken to the reasult tab, where he will be able to see a daily summary of all measurements results.

.. image:: gifs/glucose_normal.gif
           :width: 300
           :align: center

Too low
^^^^^^^^^
If the entered result is too low in relation to the correct blood sugar level, the user will be informed about it.

.. image:: gifs/glucose_low.gif
           :width: 300
           :align: center

Too high
^^^^^^^^^
If the entered result is too high in relation to the correct blood sugar level, the user will be informed about it.
He will also be able to quickly go to the insulin calculator tab, where he will be able to calculate the dose of insulin he should take to lower the sugar level.

.. image:: gifs/glucose_high.gif
           :width: 300
           :align: center

