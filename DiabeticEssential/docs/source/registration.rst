Registration
======================

Create account
--------------------

To create an account choose the appropriate option in the main view. Enter your email, password, firstname, lastname, date of birth and sex. Password must be at least 6 characters.
It is important to enter the email address to which we have access because it will be necessary to verify it.

.. image:: gifs/reg.gif
           :width: 300
           :align: center

Verify email adress
-----------------------

After registration, you will receive a message with a link to the confirmation email. Use it to complete the registration.

.. image:: images/ver1.JPG
.. image:: images/ver2.JPG
           :width: 300
           :align: center

Once you do this, you can login.