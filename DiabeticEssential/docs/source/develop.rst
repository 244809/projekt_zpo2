Future app development
=======================

One of the ideas for developing this application is adding options to set reminders to take measurements and take appropriate medications at a given time of the day.
Another improvement in the operation of the application would be the ability to connect via bluetooth with a glucometer or blood pressure monitor to be able to automatically download the measurement results.