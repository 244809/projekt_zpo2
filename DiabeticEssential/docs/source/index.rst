.. NFC Medic History documentation master file, created by
   sphinx-quickstart on Sun Jun  7 21:22:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Diabetic Essential's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   about
   registration
   login
   home
   profile
   glucose
   pressure
   reasults
   bmi
   insulin
   develop

