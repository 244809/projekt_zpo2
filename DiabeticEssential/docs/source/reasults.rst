Reasults
==========

Daily reasults
----------------

You can view your daily measurement results using the calendar view.

.. image:: gifs/reasults.gif
           :width: 300
           :align: center

Blood glucose measurements reasults
-------------------------------------
Blood glucose measurements fall into two categories: fasting and after meal (breakfast, lunch, dinner). Each measurement has an assigned time of its execution.
The results are color-coded depending on the range they are in. The division scales can be viewed by clicking the "i" icon in the lower right corner of the screen.

.. image:: images/legend.png
           :width: 300
           :align: center

Blood pressure measurements reasults
-------------------------------------
Blood pressure measurements are shown as the average of your daily measurements.


