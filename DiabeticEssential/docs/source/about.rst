About
======================

Diabetic Essential is an application aimed at people diagnosed with diabetes or problems with fluctuating blood sugar levels.
It helps in controlling the proper level of blood sugar and blood pressure.
The measurement results are stored in the firebase database.
It also comes with a BMI calculator and an insulin dose calculator.


Requirements
--------------------

To run the application you need a smartphone with Android version 7.0 or higher.


