Home
======================

Menu
------------------------------

After successful login, you will be redirected to the home page.
From this page, using the side panel, you will be able to go to the other application windows.



.. image:: gifs/menu.gif
           :width: 300
           :align: center

Reasults
------------------------------
On the home page you will find the results of your saved measurements presented in the form of scatter charts.
You can change the data view due to the type of measurement or the length of the period you want to analyze.
The graph also shows the intervals in which the results should be.

.. image:: gifs/home.gif
           :width: 300
           :align: center


Log out
------------------------------
To return to the login panel, select Log out from the side panel.

.. image:: gifs/log_out.gif
           :width: 300
           :align: center