Blood pressure measurement
============================

The user can go to this tab via the side menu or from the home view by clicking the button with the "+" icon below the graph.
The user can add pressure measurement results to his database.

Measurement
--------------------------
After entering the systolic and diastolic pressure and pressing the SUBMIT button, the result will be automatically saved to the database and it will be visible on the graph in the home view.

.. image:: gifs/pressure.gif
           :width: 300
           :align: center

Reasult
--------------------------
After pressing the REASULT button, the user will be taken to the reasult tab, where he will be able to see a daily summary of all measurements results.