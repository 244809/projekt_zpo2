Profile
======================

Personal data
------------------------------

After switching to the profile view, you can see the data entered during registration.
It is also possible to enter your weight and height, and to edit them later.

.. image:: gifs/profile.gif
           :width: 300
           :align: center

The profile icon is selected by gender:

    .. image:: images/ic_female.png
           :width: 250
           :align: left
    .. image:: images/ic_male.png
           :width: 250
           :align: right