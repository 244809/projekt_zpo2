BMI calculator
======================

Calculate
----------
The application is equipped with a BMI calculator.
If the user has saved his weight and height in the profile tab, this data will be automatically downloaded.
The user can also enter any weight and height for which the Body Mass Index will be calculated.
After clicking on the CALCULATE button the result will appear below.

.. image:: gifs/bmi.gif
           :width: 300
           :align: center

Scale BMI
----------
The results are marked according to the scale below.

.. image:: images/bmi.png
           :width: 400
           :align: center