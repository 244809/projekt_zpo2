Insulin calculator
======================

The application has a calculator to calculate the appropriate dose of insulin, which helps to normalize the glucose concentration in the body after a meal.

Carbohydrate exchanger
------------------------
1 carbohydrate exchanger (CE) is 10 g of digestible carbohydrates contained in a given portion of the product.
We receive this information after clicking on the "i" icon.
1 carbohydrate exchanger increases glycemia by about 30-50 mg / dl.

Calculate
----------
The first way
^^^^^^^^^^^^^^
Dose of insulin is calculated on the basis of appropriate individual indicators: conversion factor and correction, which are determined by the doctor.
Additionally, you should enter your current blood glucose level and the number of carbohydrate exchangers that were taken with the meal eaten before measuring the sugar level.

After clicking on the CALCULATE button the result will appear below.

.. image:: gifs/insulin1.gif
           :width: 300
           :align: center


The second way
^^^^^^^^^^^^^^^^
After clicking on the "i" icon we also receive information on how to change the method of entering data on the amount of carbohydrates consumed in the meal.
After clicking ENTER VALUES IN GRAMS, we can enter the amount of carbohydrates and fiber consumed in a 100 g meal and the amount of the product itself.
After selecting the appropriate conversion, correction and blood sugar values, you can click the CALCULATE button and get the result below.

.. image:: gifs/insulin2.gif
           :width: 300
           :align: center


Warning
^^^^^^^^^^^^^^^^
An insulin dose will not be calculated if your blood sugar is too low.
The user will also be informed that he should eat something instead of taking insulin.

.. image:: gifs/insulin3.gif
           :width: 300
           :align: center