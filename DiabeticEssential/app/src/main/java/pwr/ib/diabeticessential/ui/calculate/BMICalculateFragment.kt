package pwr.ib.diabeticessential.ui.calculate

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.objects.Patient
import pwr.ib.diabeticessential.R


class BMICalculateFragment : Fragment() {

    private var mAuth: FirebaseAuth? = null
    private var db: FirebaseFirestore? = null
    private var uid: String? = null


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        db = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
        uid = mAuth!!.getUid()
        val root = inflater.inflate(R.layout.fragment_calculate, container, false)
        val bmi1: TextView = root.findViewById(R.id.BMI)
        val editWeight: EditText = root.findViewById(R.id.weight)
        val editHeight: EditText = root.findViewById(R.id.height)
        val buttonCalculate: Button = root.findViewById(R.id.calculate)
        val seekBar: SeekBar = root.findViewById(R.id.seekBar)
        val scale: ImageView = root.findViewById(R.id.scale)

        seekBar.setVisibility(View.INVISIBLE)
        scale.setVisibility(View.INVISIBLE)
        getPersonalData(editWeight, editHeight)

        buttonCalculate.setOnClickListener() {
            bmi1.setVisibility(View.INVISIBLE)
            var BMI = editWeight.text.toString().toDouble()!! / Math.pow(
                (editHeight.text.toString().toDouble()!! / 100), 2.0
            )


            var color: Int
            var progress: Double
            if (BMI < 18.5) {
                color = resources.getColor(R.color.underweight)
            } else if (BMI < 25) {
                color = resources.getColor(R.color.normal)
            } else if (BMI < 30) {
                color = resources.getColor(R.color.overweight)
            } else if (BMI < 35) {
                color = resources.getColor(R.color.obese)
            } else {
                color = resources.getColor(R.color.extremely_obese)
            }

            if (BMI > 40) {
                progress = 400.0
            } else if (BMI < 13.6) {
                progress = 136.0
            } else if (BMI > 21 && BMI < 25) {
                progress = BMI * 10 - 14
            } else if (BMI > 25 && BMI < 30) {
                progress = BMI * 10 - 10
            } else {
                progress = BMI * 10
            }

            seekBar.setProgress(progress.toInt())
            scale.setVisibility(View.VISIBLE)
            bmi1.setTextColor(color)
            bmi1.setText(BMI.toString().substring(0, 4))
            bmi1.setVisibility(View.VISIBLE)
            seekBar.setVisibility(View.VISIBLE)
            seekBar.setEnabled(false)

        }


        return root
    }


    private fun getPersonalData(editWeight: EditText, editHeight: EditText) {

        val documentReference: DocumentReference =
            db!!.collection("patients").document(uid.toString())
        documentReference.get()
            .addOnSuccessListener(OnSuccessListener { documentSnapshots ->
                if (documentSnapshots.data == null) {
                    Toast.makeText(getActivity(), "Try again", Toast.LENGTH_LONG).show()
                } else {
                    documentReference.get()
                        .addOnSuccessListener { documentSnapshot ->
                            val patient = documentSnapshot.toObject(Patient::class.java)
                            Toast.makeText(getActivity(), "Your data", Toast.LENGTH_LONG).show()
                            if (patient!!.weight != 0.0) {
                                editWeight.setText(patient.weight.toString())
                            }
                            if (patient!!.height != 0.0) {
                                editHeight.setText(patient.height.toString())
                            }
                        }
                }
            })
            .addOnFailureListener {
                Toast.makeText(getActivity(), "Try again", Toast.LENGTH_LONG).show()
            }
    }

}
