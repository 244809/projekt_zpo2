package pwr.ib.diabeticessential.ui.home

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ProgressBar
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.github.mikephil.charting.charts.ScatterChart
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.ScatterData
import com.github.mikephil.charting.data.ScatterDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet
import com.github.mikephil.charting.jobs.ZoomJob
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.R
import java.time.LocalDate


class HomeFragment : Fragment() {

    private lateinit var valuesGlucose: ArrayList<Entry>
    private lateinit var valuesGlucose1: ArrayList<Entry>
    private lateinit var valuesGlucose2: ArrayList<Entry>
    private lateinit var valuesGlucose3: ArrayList<Entry>
    private lateinit var valuesGlucoseR: ArrayList<Entry>
    private lateinit var valuesPressureD: ArrayList<Entry>
    private lateinit var valuesPressureS: ArrayList<Entry>


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val progressBar1: ProgressBar = root.findViewById(R.id.progressBar1);
        val progressBar2: ProgressBar = root.findViewById(R.id.progressBar2);

        val mv = MyMarkerView(context, R.layout.custom_marker_view)

        val mChart1: ScatterChart = root.findViewById(R.id.chart1);
        mChart1.setTouchEnabled(true)
        mChart1.setPinchZoom(true)
        mv.setChartView(mChart1)
        mChart1.marker = mv

        val mChart2: ScatterChart = root.findViewById(R.id.chart2);
        mChart2.setTouchEnabled(true)
        mChart2.setPinchZoom(true)
        mv.setChartView(mChart2)
        mChart2.marker = mv

        var periodGlucose = 5
        var periodPressure = 5
        var glucoseName = "On empty"
        var title = "empty"

        loadData(title, periodGlucose)
        loadData("blood pressure", periodPressure)
        progressBar1.visibility = View.VISIBLE
        progressBar2.visibility = View.VISIBLE
        Handler().postDelayed({
            renderDataGlucose(mChart1, glucoseName, periodGlucose)
            renderDataPressure(mChart2, periodPressure)
            progressBar1.visibility = View.INVISIBLE
            progressBar2.visibility = View.INVISIBLE
            mChart1.visibility = View.VISIBLE
            mChart2.visibility = View.VISIBLE
        }, 3500)


        val period1: Spinner = root.findViewById(R.id.tvChartTitleOption)

        period1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (id.toInt() == 0)
                    periodGlucose = 5
                else if (id.toInt() == 1)
                    periodGlucose = 7
                else if (id.toInt() == 2)
                    periodGlucose = 14
                else if (id.toInt() == 3)
                    periodGlucose = 30

                loadData(title, periodGlucose)
                mChart1.visibility = View.INVISIBLE
                progressBar1.visibility = View.VISIBLE
                Handler().postDelayed({
                    renderDataGlucose(mChart1, glucoseName, periodGlucose)
                    progressBar1.visibility = View.INVISIBLE
                    mChart1.visibility = View.VISIBLE
                }, 3500)
            }

        }

        val period2: Spinner = root.findViewById(R.id.tvChartTitleOption2)

        period2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (id.toInt() == 0)
                    periodPressure = 5
                else if (id.toInt() == 1)
                    periodPressure = 7
                else if (id.toInt() == 2)
                    periodPressure = 14
                else if (id.toInt() == 3)
                    periodPressure = 30

                loadData("blood pressure", periodPressure)
                mChart2.visibility = View.INVISIBLE
                progressBar2.visibility = View.VISIBLE
                Handler().postDelayed({
                    renderDataPressure(mChart2, periodPressure)
                    progressBar2.visibility = View.INVISIBLE
                    mChart2.visibility = View.VISIBLE
                }, 3500)
            }
        }


        val radioGroup: RadioGroup = root.findViewById(R.id.radioGroup)
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            mChart1.clear()
            if (checkedId == R.id.on_empty) {
                glucoseName = "On empty"
                title = "empty"
            } else if (checkedId == R.id.random) {
                glucoseName = "Random"
                title = "random"
            } else if (checkedId == R.id.meal) {
                glucoseName = "After meal"
                title = "meal"
            }

            loadData(title, periodGlucose)
            mChart1.visibility = View.INVISIBLE
            progressBar1.visibility = View.VISIBLE
            Handler().postDelayed({
                renderDataGlucose(mChart1, glucoseName, periodGlucose)
                progressBar1.visibility = View.INVISIBLE
                mChart1.visibility = View.VISIBLE
            }, 3500)


        }


        val fab: FloatingActionButton = root.findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Navigation.findNavController(root).navigate(R.id.nav_glucose)
        }

        val fab2: FloatingActionButton = root.findViewById(R.id.fab2)
        fab2.setOnClickListener { view ->
            Navigation.findNavController(root).navigate(R.id.nav_pressure)
        }


        return root
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun renderDataGlucose(mChart: ScatterChart, dataName: String, period: Int) {

        mChart.clear()

        var maxLim: Float
        if (dataName.equals("On empty")) {
            maxLim = 100f
        } else if (dataName.equals("After meal")) {
            maxLim = 140f
        } else {
            maxLim = 0f
        }

        var ll1 = LimitLine(maxLim)
        ll1.label = "Maximum Limit"
        ll1.lineWidth = 1f
        ll1.enableDashedLine(15f, 5f, 0f)
        ll1.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        ll1.textSize = 7f
        ll1.lineColor = Color.RED

        var ll2 = LimitLine(70f)
        ll2.label = "Minimum Limit"
        ll2.lineWidth = 1f
        ll2.enableDashedLine(15f, 5f, 0f)
        ll2.labelPosition = LimitLine.LimitLabelPosition.RIGHT_BOTTOM
        ll2.textSize = 7f
        ll2.lineColor = Color.BLUE


        val leftAxis: YAxis = mChart.getAxisLeft()
        if (dataName.equals("Random")) {
            leftAxis.removeAllLimitLines()
        } else {
            leftAxis.removeAllLimitLines()
            leftAxis.addLimitLine(ll1)
            leftAxis.addLimitLine(ll2)
        }
        leftAxis.axisMaximum = 200f
        leftAxis.axisMinimum = 40f

        val xAxis: XAxis = mChart.getXAxis()
        xAxis.axisMaximum = (period + 1).toFloat()
        xAxis.axisMinimum = 0F

        mChart.getAxisRight().setEnabled(false)
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        mChart.setVisibleXRangeMaximum((period + 1).toFloat());
        mChart.moveViewToX(0f)
        mChart.zoom(0f,0f,0f,0f)
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        val xAxes = labels(period)
        mChart.xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            val index = value.coerceAtMost(axis.axisMaximum).toInt()
            xAxes[index]
        }

        if (dataName.equals("On empty")) {
            setDataGlucose1(mChart)
        } else if (dataName.equals("After meal")) {
            setDataGlucose2(mChart)
        } else if (dataName.equals("Random")) {
            setDataGlucose3(mChart)
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun renderDataPressure(mChart: ScatterChart, period: Int) {

        mChart.clear()

        var ll1: LimitLine
        var ll2: LimitLine
        val leftAxis: YAxis = mChart.getAxisLeft()

        ll1 = LimitLine(80f, "Diastolic")
        ll2 = LimitLine(120f, "Systolic")
        ll1.lineColor = Color.GRAY
        ll2.lineColor = Color.DKGRAY
        leftAxis.axisMaximum = 150f
        leftAxis.axisMinimum = 60f

        ll1.lineWidth = 1f
        ll1.enableDashedLine(15f, 5f, 0f)
        ll1.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        ll1.textSize = 7f

        ll2.lineWidth = 1f
        ll2.enableDashedLine(15f, 5f, 0f)
        ll2.labelPosition = LimitLine.LimitLabelPosition.RIGHT_BOTTOM
        ll2.textSize = 7f

        leftAxis.removeAllLimitLines()
        leftAxis.addLimitLine(ll1)
        leftAxis.addLimitLine(ll2)

        val xAxis: XAxis = mChart.getXAxis()
        xAxis.axisMaximum = (period + 1).toFloat()
        xAxis.axisMinimum = 0F

        mChart.getAxisRight().setEnabled(false)
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        mChart.setVisibleXRangeMaximum((period + 1).toFloat())
        mChart.moveViewToX(0f)
        mChart.zoom(0f,0f,0f,0f)
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        val xAxes = labels(period)
        mChart.xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            val index = value.coerceAtMost(axis.axisMaximum).toInt()
            xAxes[index]
        }

        setDataPressure(mChart, "blood pressure")
    }


    fun setDataGlucose1(mChart: ScatterChart) {
        val values: ArrayList<Entry> = valuesGlucose
        val set1: ScatterDataSet
        set1 = ScatterDataSet(values, "Fasting")
        set1.color = Color.DKGRAY
        set1.valueTextSize = 9f
        val dataSets: ArrayList<IScatterDataSet> = ArrayList()
        dataSets.add(set1)
        val data = ScatterData(dataSets)
        mChart.data = data
    }

    fun setDataGlucose2(mChart: ScatterChart) {

        val values1: ArrayList<Entry> = valuesGlucose1
        val values2: ArrayList<Entry> = valuesGlucose2
        val values3: ArrayList<Entry> = valuesGlucose3
        val set1: ScatterDataSet
        val set2: ScatterDataSet
        val set3: ScatterDataSet
        set1 = ScatterDataSet(values1, "After breakfast")
        set1.color = Color.YELLOW
        set1.valueTextSize = 9f
        set2 = ScatterDataSet(values2, "After dinner")
        set2.color = Color.GREEN
        set2.valueTextSize = 9f
        set3 = ScatterDataSet(values3, "After supper")
        set3.color = Color.MAGENTA
        set3.valueTextSize = 9f

        val dataSets: ArrayList<IScatterDataSet> = ArrayList()
        dataSets.add(set1)
        dataSets.add(set2)
        dataSets.add(set3)
        val data = ScatterData(dataSets)
        mChart.data = data
    }

    fun setDataGlucose3(mChart: ScatterChart) {
        val values: ArrayList<Entry> = valuesGlucoseR
        val set1: ScatterDataSet
        set1 = ScatterDataSet(values, "Random")
        set1.color = Color.GRAY
        set1.valueTextSize = 9f
        val dataSets: ArrayList<IScatterDataSet> = ArrayList()
        dataSets.add(set1)
        val data = ScatterData(dataSets)
        mChart.data = data
    }

    fun setDataPressure(mChart: ScatterChart, dataName: String) {
        val values1: ArrayList<Entry> = valuesPressureD
        val values2: ArrayList<Entry> = valuesPressureS

        val set1: ScatterDataSet
        set1 = ScatterDataSet(values1, "Diastolic " + dataName)
        set1.color = Color.BLUE
        set1.valueTextSize = 9f
        val set2: ScatterDataSet
        set2 = ScatterDataSet(values2, "Systolic " + dataName)
        set2.color = Color.RED
        set2.valueTextSize = 9f
        val dataSets: ArrayList<IScatterDataSet> = ArrayList()
        dataSets.add(set1)
        dataSets.add(set2)
        val data = ScatterData(dataSets)
        mChart.data = data

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun loadData(title: String, period: Int) {

        if (title.equals("empty")) {
            val data = LoadData(title, period)
            data.execute()
            valuesGlucose = ArrayList()
            valuesGlucose = data.get()
        } else if (title.equals("meal")) {
            val data1 = LoadData(title + " - breakfast", period)
            val data2 = LoadData(title + " - dinner", period)
            val data3 = LoadData(title + " - supper", period)
            data1.execute()
            data2.execute()
            data3.execute()
            valuesGlucose1 = ArrayList()
            valuesGlucose2 = ArrayList()
            valuesGlucose3 = ArrayList()
            valuesGlucose1 = data1.get()
            valuesGlucose2 = data2.get()
            valuesGlucose3 = data3.get()
        } else if (title.equals("random")) {
            val dataR = LoadData(title, period)
            dataR.execute()
            valuesGlucoseR = ArrayList()
            valuesGlucoseR = dataR.get()
        } else {
            val dataD = LoadData("diastolic blood pressure", period)
            val dataS = LoadData("systolic blood pressure", period)
            dataD.execute()
            dataS.execute()
            valuesPressureD = ArrayList()
            valuesPressureS = ArrayList()
            valuesPressureD = dataD.get()
            valuesPressureS = dataS.get()
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun labels(period: Int): ArrayList<String> {

        val today: LocalDate = LocalDate.now().minusDays(period.toLong())
        val xAxes: ArrayList<String> = ArrayList()
        xAxes.add(0, "")
        for (i in 1 until period + 1) {
            val day = today.plusDays(i.toLong())
            xAxes.add(i, day.dayOfMonth.toString() + "." + day.monthValue)
        }
        xAxes.add(period + 1, "")

        return xAxes
    }

}







