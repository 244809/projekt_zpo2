package pwr.ib.diabeticessential.ui.pressure

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.objects.MultiMeasurement
import pwr.ib.diabeticessential.R
import java.time.LocalDate
import java.time.LocalTime

class PressureFragment : Fragment() {
    private var mAuth: FirebaseAuth? = null
    private var db: FirebaseFirestore? = null
    private var uid: String? = null
    lateinit var time: String
    lateinit var date: String

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        db = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
        uid = mAuth!!.getUid()
        val root = inflater.inflate(R.layout.fragment_pressure, container, false)
        val dias: TextView = root.findViewById(R.id.diasPressure)
        val sys: TextView = root.findViewById(R.id.sysPressure)
        val result: Button = root.findViewById(R.id.results)
        val submit: Button = root.findViewById(R.id.submit)



        submit.setOnClickListener() {

            date = LocalDate.now().toString()
            time = LocalTime.now().toString().substring(0, 2) + ":" + LocalTime.now().toString()
                .substring(3, 5)
            if (!dias.text.isEmpty() && !sys.text.isEmpty()) {
                saveToBase(sys.text.toString(), dias.text.toString())
                hideKeyboardFrom(requireContext().applicationContext, requireView())
            } else {
                Toast.makeText(
                    getActivity(),
                    "You need to enter the measurement result",
                    Toast.LENGTH_LONG
                ).show()
            }

        }

        result.setOnClickListener() {
            Navigation.findNavController(root).navigate(R.id.nav_result)
        }

        return root
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun saveToBase(sys: String, dias: String) {
        val db = FirebaseFirestore.getInstance()
        var DocRef = db.collection("pressure").document(mAuth!!.getUid().toString())
            .collection("pressure").document(date)


        DocRef.get().addOnSuccessListener {
            if (!it.exists()) {
                val measurement =
                    MultiMeasurement(
                        arrayListOf(
                            sys
                        ), arrayListOf(dias), date, arrayListOf(time)
                    )
                DocRef.set(measurement)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(activity, "Reasult was saved", Toast.LENGTH_LONG)
                                .show()
                        } else {
                            Toast.makeText(activity, "Reasult was not saved", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
            } else {
                DocRef.update("diastolicPressure", FieldValue.arrayUnion(dias))
                DocRef.update("systolicPressure", FieldValue.arrayUnion(sys))
                DocRef.update("time", FieldValue.arrayUnion(time))

                Toast.makeText(activity, "Reasult was saved", Toast.LENGTH_LONG)
                    .show()
            }
        }

    }


}
