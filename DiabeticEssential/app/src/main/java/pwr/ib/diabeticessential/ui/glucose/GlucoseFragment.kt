package pwr.ib.diabeticessential.ui.glucose

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.objects.Measurement
import pwr.ib.diabeticessential.objects.MultiMeasurement
import pwr.ib.diabeticessential.R
import java.time.LocalDate
import java.time.LocalTime


class GlucoseFragment : Fragment() {

    private var mAuth: FirebaseAuth? = null
    private var db: FirebaseFirestore? = null
    private var uid: String? = null
    lateinit var title: String
    lateinit var time: String
    lateinit var date: String
    var snackbar: Snackbar? = null


    override fun onDestroyView() {
        if (snackbar != null) {
            if (snackbar!!.isShown)
                snackbar!!.dismiss()
        }
        super.onDestroyView()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        db = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
        uid = mAuth!!.getUid()
        val root = inflater.inflate(R.layout.fragment_glucose, container, false)
        val radioGroup: RadioGroup = root.findViewById(R.id.radioGroup)
        val glucose: TextView = root.findViewById(R.id.glucoseMeasurement)
        val result: Button = root.findViewById(R.id.results)
        val submit: Button = root.findViewById(R.id.submit)



        submit.setOnClickListener() {
            val id = radioGroup.checkedRadioButtonId
            date = LocalDate.now().toString()
            time = LocalTime.now().toString().substring(0, 2) + ":" + LocalTime.now().toString()
                .substring(3, 5)
            title = ""
            if (!glucose.text.isEmpty()) {
                if (id != -1) {
                    if (id == R.id.on_empty) {
                        title = "empty"
                    } else if (id == R.id.random) {
                        title = "random"
                    } else {
                        title = "meal"
                        if (id == R.id.dinner) {
                            title = title + " - dinner"
                        } else if (id == R.id.supper) {
                            title = title + " - supper"
                        } else if (id == R.id.breakfast) {
                            title = title + " - breakfast"
                        }
                    }
                    val level = glucose.text.toString()
                    saveToBase(level)
                    hideKeyboardFrom(requireContext().applicationContext, requireView())
                    if ((level.toInt() > 100 && title.equals("empty")) || (level.toInt() > 140 && title.contains(
                            "meal"
                        ))
                    ) {
                        snackbar = Snackbar.make(
                            requireView(),
                            "You should take insulin",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snackbar!!.setAction("Go to Insulin calculator",
                            { v -> Navigation.findNavController(root).navigate(R.id.nav_insulin) })
                            .show()
                    } else if (level.toInt() < 70) {
                        snackbar = Snackbar.make(
                            requireView(),
                            "You should have a snack",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snackbar!!.show()
                    }
                } else {
                    Toast.makeText(
                        getActivity(),
                        "You need to select the type of measurement",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    getActivity(),
                    "You need to enter the measurement result",
                    Toast.LENGTH_LONG
                ).show()
            }

        }

        result.setOnClickListener() {
            Navigation.findNavController(root).navigate(R.id.nav_result)
        }

        return root
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun saveToBase(result: String) {

        val db = FirebaseFirestore.getInstance()
        var DocRef = db.collection("glucose").document(mAuth!!.getUid().toString())
            .collection(title).document(date)



        if (title.equals("random")) {
            DocRef.get().addOnSuccessListener {
                if (!it.exists()) {
                    val measurement =
                        MultiMeasurement(
                            arrayListOf(result),
                            date,
                            arrayListOf(time)
                        )
                    DocRef.set(measurement)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(activity, "Reasult was saved", Toast.LENGTH_LONG)
                                    .show()
                            } else {
                                Toast.makeText(activity, "Reasult was not saved", Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                } else {
                    DocRef.update("glucose", FieldValue.arrayUnion(result))
                    DocRef.update("time", FieldValue.arrayUnion(time))

                    Toast.makeText(activity, "Reasult was saved", Toast.LENGTH_LONG)
                        .show()
                }
            }
        } else {
            val measurement =
                Measurement(result, date, time)
            DocRef.set(measurement)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "Reasult was saved", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, "Reasult was not saved", Toast.LENGTH_LONG).show()
                    }
                }
        }

    }


}
