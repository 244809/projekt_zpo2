package pwr.ib.diabeticessential.ui.calculate

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.R


class InsulinCalculateFragment : Fragment() {

    private var mAuth: FirebaseAuth? = null
    private var db: FirebaseFirestore? = null
    private var uid: String? = null
    var snackbar: Snackbar? = null


    override fun onDestroyView() {
        if (snackbar != null) {
            if (snackbar!!.isShown)
                snackbar!!.dismiss()
        }
        super.onDestroyView()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        db = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
        uid = mAuth!!.getUid()
        var start = true
        var show = true
        val root = inflater.inflate(R.layout.fragment_insulin, container, false)
        val buttonCalculate: Button = root.findViewById(R.id.calculate)
        val ceEditText: EditText = root.findViewById(R.id.ce)
        val ceUnit: TextView = root.findViewById(R.id.unitCE)
        val glucoseEditText: EditText = root.findViewById(R.id.glucose)
        val convertionSpinner: Spinner = root.findViewById(R.id.converter)
        val correctionSpinner: Spinner = root.findViewById(R.id.correction)
        val carbo: TextInputLayout = root.findViewById(R.id.ch)
        val fiber: TextInputLayout = root.findViewById(R.id.b)
        val amount: TextInputLayout = root.findViewById(R.id.il)

        val fab1: FloatingActionButton = root.findViewById(R.id.fab1)
        fab1.setOnClickListener { view ->
            hideKeyboardFrom(requireContext().applicationContext, view)
            if (show) {
                val info: String
                if (start) {
                    info = "Enter values in grams"
                } else {
                    info = "Enter values in CE"
                }
                snackbar = Snackbar.make(
                    view,
                    "1 CE = 10g of digestible carbohydrates",
                    Snackbar.LENGTH_INDEFINITE
                )
                snackbar!!.setAction(info) {
                    show = true
                    if (start) {
                        carbo.visibility = View.VISIBLE
                        fiber.visibility = View.VISIBLE
                        amount.visibility = View.VISIBLE
                        ceEditText.visibility = View.INVISIBLE
                        ceUnit.visibility = View.INVISIBLE
                        start = false
                    } else {
                        carbo.visibility = View.INVISIBLE
                        fiber.visibility = View.INVISIBLE
                        amount.visibility = View.INVISIBLE
                        ceEditText.visibility = View.VISIBLE
                        ceUnit.visibility = View.VISIBLE
                        start = true
                    }
                }

                snackbar!!.show()
                show = false
            } else {
                snackbar!!.dismiss()
                show = true
            }
        }

        buttonCalculate.setOnClickListener() { view ->
            hideKeyboardFrom(requireContext().applicationContext, view)
            if ((ceEditText.text.isEmpty() && start) || (!start && (carbo.editText!!.text.isEmpty() || fiber.editText!!.text.isEmpty() || amount.editText!!.text.isEmpty())) || glucoseEditText.text.isEmpty()) {
                Toast.makeText(context, "Please fill all the fields", Toast.LENGTH_LONG).show()
            } else {
                val glucose = glucoseEditText.text.toString().toDouble()
                if (glucose<70) {
                    snackbar = Snackbar.make(
                        view,
                        "You should have a snack",
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackbar!!.show()
                } else {
                    val ce: Double
                    if (start) {
                        ce = ceEditText.text.toString().toDouble()
                    } else {
                        val ch = carbo.editText!!.text.toString().toDouble()
                        val f = fiber.editText!!.text.toString().toDouble()
                        val p = amount.editText!!.text.toString().toDouble()
                        ce = ((ch - f) * p) / 1000
                    }
                    val converter = convertionSpinner.selectedItem.toString().toDouble()
                    var correction = 0

                    if (glucose >= 100) {
                        correction = ((glucose - 100) / correctionSpinner.selectedItem.toString()
                            .toDouble()).toInt()
                    }

                    val insulin = Math.round(ce * converter + correction)

                    snackbar = Snackbar.make(
                        view,
                        "You should take " + insulin + " units of insulin",
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackbar!!.show()
                }



            }

        }


        return root
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}
