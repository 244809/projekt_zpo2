package pwr.ib.diabeticessential.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.objects.Patient
import pwr.ib.diabeticessential.R
import java.time.LocalDate


class RegActivity : AppCompatActivity() {
    private var emailEt: EditText? = null
    private var passwordEt: EditText? = null
    private var firstnameEt: EditText? = null
    private var lastnameEt: EditText? = null
    private var birthdateEt: DatePicker? = null
    private var sexS: Spinner? = null

    private var mAuth: FirebaseAuth? = null
    var email: String? = null
    var password: String? = null
    var firstname: String? = null
    var lastname: String? = null
    var sex: String? = null
    var birthdate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mAuth = FirebaseAuth.getInstance()
        emailEt = findViewById(R.id.email_edt_text)
        passwordEt = findViewById(R.id.pass_edt_text)
        firstnameEt = findViewById(R.id.firstname_edt_text)
        lastnameEt = findViewById(R.id.lastname_edt_text)
        sexS = findViewById(R.id.sex)
        birthdateEt = findViewById(R.id.birthdate)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun onClickSingUp(v: View) {
        email = emailEt!!.text.toString()
        password = passwordEt!!.text.toString()
        firstname = firstnameEt!!.text.toString()
        lastname = lastnameEt!!.text.toString()
        sex = sexS!!.selectedItem.toString()
        birthdate = LocalDate.of(birthdateEt!!.year, birthdateEt!!.month, birthdateEt!!.dayOfMonth).toString()
        if (email!!.isEmpty() || password!!.isEmpty() || firstname!!.isEmpty() || lastname!!.isEmpty()) {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_LONG).show()
        } else {
            mAuth!!.createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        mAuth!!.currentUser!!.sendEmailVerification()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Toast.makeText(
                                        this@RegActivity,
                                        "Successfully Registered. Check your email to verify account.",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    val patient =
                                        Patient(
                                            firstname,
                                            lastname,
                                            sex,
                                            birthdate
                                        )
                                    val newPatient = mAuth!!.currentUser
                                    val newID = newPatient!!.uid
                                    addPatiensData(patient, newID)
                                    val intent =
                                        Intent(this@RegActivity, LogActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        this@RegActivity,
                                        task.exception!!.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                    } else {
                        Toast.makeText(
                            this@RegActivity,
                            "Registration Failed" + " " + task.exception!!.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
        }
    }

    private fun addPatiensData(patient: Patient, uid: String) {
        val db = FirebaseFirestore.getInstance()
        val DocRef =
            db.collection("patients").document(uid)
        DocRef.set(patient)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "New patient added")
                } else {
                    Log.d(TAG, "Failed in patient adding")
                }
            }
    }

    companion object {
        val TAG = RegActivity::class.java.simpleName
    }
}
