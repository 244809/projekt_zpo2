package pwr.ib.diabeticessential.ui.profile

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import pwr.ib.diabeticessential.objects.Patient
import pwr.ib.diabeticessential.R


class ProfileFragment : Fragment() {

    private var mAuth: FirebaseAuth? = null
    private var db: FirebaseFirestore? = null
    private var uid: String? = null


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        db = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
        uid = mAuth!!.getUid()
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        val buttonSave: Button = root.findViewById(R.id.save)
        val profile: CardView = root.findViewById(R.id.profile)
        val avatar: ImageView = root.findViewById(R.id.icon)
        profile.setVisibility(View.INVISIBLE)
        avatar.setVisibility(View.INVISIBLE)

        val name: EditText = root.findViewById(R.id.etName)
        val birth: EditText = root.findViewById(R.id.etBirth)
        val weight: EditText = root.findViewById(R.id.etWeight)
        val height: EditText = root.findViewById(R.id.etHeight)

        val editTextViews: Array<EditText> = arrayOf(name, birth, weight, height)
        for (i in 0 until 4) {
            editTextViews[i].setEnabled(false)
            editTextViews[i].setTextColor(Color.BLACK)
        }
        getPersonalData(profile, name, birth, weight, height, avatar)

        buttonSave.setOnClickListener() {
            if (buttonSave.text.equals("Edit")) {
                buttonSave.setText("Save")
                height.setEnabled(true)
                weight.setEnabled(true)
            } else {
                if (height.text.isNotEmpty() && weight.text.isNotEmpty()) {
                    changeData(height, weight)
                } else {
                    Toast.makeText(
                        getActivity(),
                        "Fill in the weight and height fields",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                buttonSave.setText("Edit")
                height.setEnabled(false)
                weight.setEnabled(false)
            }
        }

        return root
    }

    private fun changeData(editHeight: EditText, editWeight: EditText) {

        val documentReference: DocumentReference =
            db!!.collection("patients").document(uid.toString())
        documentReference.get()
            .addOnSuccessListener { documentSnapshot ->
                val patient = documentSnapshot.toObject(Patient::class.java)!!

                patient.weight = editWeight.getText().toString().toDouble()
                patient.height = editHeight.getText().toString().toDouble()

                documentReference.set(patient)
                    .addOnCompleteListener { task: Task<Void?> ->
                        if (!task.isSuccessful) {
                            Toast.makeText(getActivity(), "Try again", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(getActivity(), "New data saved", Toast.LENGTH_LONG)
                                .show()
                        }
                    }

            }

    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPersonalData(
        profile: CardView,
        name: TextView,
        birth: TextView,
        weight: TextView,
        height: TextView,
        avatar: ImageView
    ) {

        val documentReference: DocumentReference =
            db!!.collection("patients").document(uid.toString())
        documentReference.get()
            .addOnSuccessListener(OnSuccessListener { documentSnapshots ->
                if (documentSnapshots.data == null) {
                    Toast.makeText(getActivity(), "Try again", Toast.LENGTH_LONG).show()
                } else {
                    documentReference.get()
                        .addOnSuccessListener { documentSnapshot ->
                            val patient = documentSnapshot.toObject(Patient::class.java)
                            Toast.makeText(getActivity(), "Your data", Toast.LENGTH_LONG).show()
                            name.setText(patient!!.firstname + " " + patient!!.lastname)
                            birth.setText(
                                patient!!.birth + " (" + patient!!.calculateAge().toString() + ")"
                            )
                            weight.setText(patient!!.weight.toString())
                            height.setText(patient!!.height.toString())
                            if (patient.sex.equals("Male")) {
                                avatar.setImageResource(R.drawable.ic_male)
                            } else {
                                avatar.setImageResource(R.drawable.ic_female)
                            }
                            profile.setVisibility(View.VISIBLE)
                            avatar.setVisibility(View.VISIBLE)
                        }
                }
            })
            .addOnFailureListener {
                Toast.makeText(getActivity(), "Try again", Toast.LENGTH_LONG).show()
            }

    }

}
