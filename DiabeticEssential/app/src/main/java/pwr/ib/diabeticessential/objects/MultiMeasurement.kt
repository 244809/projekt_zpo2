package pwr.ib.diabeticessential.objects

class MultiMeasurement {

    var glucose: ArrayList<String>? = null
    var systolicPressure: ArrayList<String>? = null
    var diastolicPressure: ArrayList<String>? = null
    var date: String? = null
    var time: ArrayList<String>? = null

    constructor() {}

    constructor(glucose: ArrayList<String>?, date: String?, time: ArrayList<String>?) {
        this.glucose = glucose
        this.date = date
        this.time = time
    }

    constructor(
        systolicPressure: ArrayList<String>?,
        diastolicPressure: ArrayList<String>?,
        date: String?,
        time: ArrayList<String>?
    ) {
        this.systolicPressure = systolicPressure
        this.diastolicPressure = diastolicPressure
        this.date = date
        this.time = time
    }


}