package pwr.ib.diabeticessential.ui.home

import android.os.AsyncTask
import android.os.Build
import androidx.annotation.RequiresApi
import com.github.mikephil.charting.data.Entry
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import pwr.ib.diabeticessential.objects.Measurement
import pwr.ib.diabeticessential.objects.MultiMeasurement
import java.time.LocalDate
import kotlin.collections.ArrayList

class LoadData(private val title: String, private val perioid: Int) :
    AsyncTask<String, Int, ArrayList<Entry>>() {


    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()


    @RequiresApi(Build.VERSION_CODES.O)
    override fun doInBackground(vararg params: String?): ArrayList<Entry> {
        val before = LocalDate.now().minusDays((perioid - 1).toLong())
        val dates: ArrayList<LocalDate> = arrayListOf()
        for (i in 0..perioid - 1) {
            dates.add(before.plusDays(i.toLong()))
        }
        val DocRef: DocumentReference
        val task: Task<QuerySnapshot>
        var values: ArrayList<Entry> = arrayListOf()
        if (title.contains("blood pressure")) {
            DocRef = db.collection("pressure").document(mAuth!!.getUid().toString())
            task =
                DocRef.collection("pressure").whereGreaterThanOrEqualTo("date", before.toString())
                    .orderBy("date", Query.Direction.ASCENDING).limit(perioid.toLong()).get()
            values = dataRandomAndPressure(task, dates, title)
        } else {
            DocRef = db.collection("glucose").document(mAuth!!.getUid().toString())
            task = DocRef.collection(title).whereGreaterThanOrEqualTo("date", before.toString())
                .orderBy("date", Query.Direction.ASCENDING).limit(perioid.toLong()).get()
            if (title.equals("random")) {
                values = dataRandomAndPressure(task, dates, title)
            } else {
                values = dataEmptyAndAfterMeal(task, dates)
            }
        }


        return values
    }

    private fun dataEmptyAndAfterMeal(
        task: Task<QuerySnapshot>,
        dates: ArrayList<LocalDate>
    ): ArrayList<Entry> {
        val valuesEAAM = ArrayList<Entry>()
        task.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val documentResults = task.result?.toObjects(Measurement::class.java)
                if (!documentResults.isNullOrEmpty()) {
                    var i = 0
                    var found: Boolean
                    for (date in dates) {
                        found = false
                        for (document in documentResults!!) {
                            if (document.date!!.equals(date.toString())) {
                                val glucoseResult = document.glucose.toString()
                                valuesEAAM.add(Entry(i.toFloat() + 1, glucoseResult.toFloat()))
                                found = true
                                i++
                            }
                        }
                        if (!found)
                            i++
                    }
                } else {
                    valuesEAAM.add(Entry(-1f, 0f))
                }
            } else {
                println("Error")
            }
        }

        return valuesEAAM
    }

    private fun dataRandomAndPressure(
        task: Task<QuerySnapshot>,
        dates: ArrayList<LocalDate>,
        title: String
    ): ArrayList<Entry> {
        val valuesRAP = ArrayList<Entry>()
        task.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val documentResults = task.result?.toObjects(MultiMeasurement::class.java)
                if (!documentResults.isNullOrEmpty()) {
                    var i = 0
                    var found: Boolean
                    for (date in dates) {
                        found = false
                        for (document in documentResults!!) {
                            if (document.date!!.equals(date.toString())) {
                                if (title.equals("random")) {
                                    val length = document.glucose!!.size
                                    for (j in 0..length - 1) {
                                        val result = document.glucose!![j]
                                        val time = document.time!![j]
                                        valuesRAP.add(
                                            Entry(
                                                i.toFloat() + 1 + timePlus(time),
                                                result.toFloat()
                                            )
                                        )
                                    }
                                } else if (title.contains("diastolic")) {
                                    val length = document.diastolicPressure!!.size
                                    for (j in 0..length - 1) {
                                        val result = document.diastolicPressure!![j]
                                        val time = document.time!![j]
                                        valuesRAP.add(
                                            Entry(
                                                i.toFloat() + 1 + timePlus(time),
                                                result.toFloat()
                                            )
                                        )
                                    }
                                } else if (title.contains("systolic")) {
                                    val length = document.systolicPressure!!.size
                                    for (j in 0..length - 1) {
                                        val result = document.systolicPressure!![j]
                                        val time = document.time!![j]
                                        valuesRAP.add(
                                            Entry(
                                                i.toFloat() + 1 + timePlus(time),
                                                result.toFloat()
                                            )
                                        )
                                    }
                                }
                                found = true
                                i++
                            }
                        }
                        if (!found)
                            i++
                    }
                } else {
                    valuesRAP.add(Entry(-1f, 0f))
                }
            } else {
                println("Error")
            }

        }

        return valuesRAP
    }

    private fun timePlus(tmp: String): Float {
        var result = 0f

        var hours = tmp.substring(0, 2).toDouble()
        var minutes = tmp.substring(3, 5).toDouble()
        result = ((hours * 60.0 + minutes) / 1440.0).toFloat()


        return result
    }

}