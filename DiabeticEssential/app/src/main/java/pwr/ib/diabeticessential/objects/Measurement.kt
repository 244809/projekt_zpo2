package pwr.ib.diabeticessential.objects

class Measurement {

    var glucose: String? = null
    var systolicPressure: String? = null
    var diastolicPressure: String? = null
    var date: String? = null
    var time: String? = null

    constructor() {}

    constructor(glucose: String?, date: String?, time: String?) {
        this.glucose = glucose
        this.date = date
        this.time = time
    }

    constructor(
        systolicPressure: String?,
        diastolicPressure: String?,
        date: String?,
        time: String?
    ) {
        this.systolicPressure = systolicPressure
        this.diastolicPressure = diastolicPressure
        this.date = date
        this.time = time
    }


}