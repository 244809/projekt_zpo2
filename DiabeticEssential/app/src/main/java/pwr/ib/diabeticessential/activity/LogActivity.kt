package pwr.ib.diabeticessential.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import pwr.ib.diabeticessential.R

class LogActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    var emailEt: EditText? = null
    var passwordEt: EditText? = null

    override fun onStart() {
        super.onStart()
        mAuth = FirebaseAuth.getInstance()
        val logged =
            mAuth!!.getCurrentUser() != null && mAuth!!.getCurrentUser()!!.isEmailVerified
        if (logged) {
            val intent = Intent(this@LogActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        emailEt = findViewById<EditText>(R.id.email_edt_text)
        passwordEt = findViewById<EditText>(R.id.pass_edt_text)
    }

    fun onClickSingUp(v: View) {
        val intent = Intent(this@LogActivity, RegActivity::class.java)
        startActivity(intent)
    }

    fun onClickLogin(v: View) {
        val email = emailEt!!.text.toString()
        val password = passwordEt!!.text.toString()
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this@LogActivity, "Please fill all the fields", Toast.LENGTH_LONG).show()
        } else {
            mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                        if (task.isSuccessful()) {
                            if (mAuth!!.getCurrentUser()!!.isEmailVerified()) {
                                Toast.makeText(
                                    this@LogActivity,
                                    "Successfully log in",
                                    Toast.LENGTH_LONG
                                ).show()
                                val intent = Intent(this@LogActivity, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(
                                    this@LogActivity,
                                    "Please verify your email",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        } else {
                            Toast.makeText(this@LogActivity, "Log in failed", Toast.LENGTH_LONG)
                                .show()
                        }
                }
        }
    }

    fun onClickForget(v: View) {
        val email = emailEt!!.text.toString()
        if (email.contains("@") && email.length >= 5) {
            mAuth!!.sendPasswordResetEmail(email)
                .addOnCompleteListener(object : OnCompleteListener<Void?> {
                    override fun onComplete(task: Task<Void?>) {
                        if (task.isSuccessful()) {
                            Toast.makeText(
                                this@LogActivity,
                                "Password reset link send to your email",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Toast.makeText(
                                this@LogActivity,
                                task.getException()!!.message,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                })
        } else Toast.makeText(
            this@LogActivity,
            "Type correct mail to get password reset.",
            Toast.LENGTH_LONG
        ).show()
    }

}
