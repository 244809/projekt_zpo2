package pwr.ib.diabeticessential.ui.results

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import pwr.ib.diabeticessential.objects.Measurement
import pwr.ib.diabeticessential.objects.MultiMeasurement
import pwr.ib.diabeticessential.R
import java.time.LocalDate
import kotlin.collections.ArrayList


class DayResultFragment : Fragment() {

    private lateinit var mAuth: FirebaseAuth
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()
    var snackbar: Snackbar? = null


    override fun onDestroyView() {
        if (snackbar != null) {
            if (snackbar!!.isShown)
                snackbar!!.dismiss()
        }
        super.onDestroyView()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mAuth = FirebaseAuth.getInstance()

        val root = inflater.inflate(R.layout.fragment_day_result, container, false)

        val empty: TextView = root.findViewById(R.id.on_empty_result)
        val breafast: TextView = root.findViewById(R.id.breakfast_result)
        val dinner: TextView = root.findViewById(R.id.dinner_result)
        val supper: TextView = root.findViewById(R.id.supper_result)
        val pressure: TextView = root.findViewById(R.id.pressure_result)

        val emptyTime: TextView = root.findViewById(R.id.on_empty_time)
        val breafastTime: TextView = root.findViewById(R.id.breakfast_time)
        val dinnerTime: TextView = root.findViewById(R.id.dinner_time)
        val supperTime: TextView = root.findViewById(R.id.supper_time)

        val calendarView: CalendarView = root.findViewById(R.id.calendarView);
        var today = LocalDate.now()
        val textViews: ArrayList<TextView> = arrayListOf(
            empty,
            breafast,
            dinner,
            supper,
            emptyTime,
            breafastTime,
            dinnerTime,
            supperTime,
            pressure
        )

        calendarView.firstDayOfWeek = 2

        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            today = LocalDate.of(year, month.plus(1), dayOfMonth)
            everyResultLoad(today.toString(), textViews)
        }

        everyResultLoad(today.toString(), textViews)

        val fab: FloatingActionButton = root.findViewById(R.id.fab1)
        fab.setOnClickListener { view ->
            snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE)
            snackbar!!.setAction("X", { v -> snackbar!!.dismiss() })
            val text: TextView =
                snackbar!!.view.findViewById(com.google.android.material.R.id.snackbar_text)
            text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.legend3, 0, 0, 0)
            snackbar!!.show()

        }


        return root
    }


    private fun everyResultLoad(date: String, textViews: ArrayList<TextView>) {
        glucoseResults(date, "empty", 0, textViews)
        glucoseResults(date, "meal - breakfast", 1, textViews)
        glucoseResults(date, "meal - dinner", 2, textViews)
        glucoseResults(date, "meal - supper", 3, textViews)
        pressureResults(date, 8, textViews)
    }

    private fun checkOnEmpty(textView: TextView, level: Double) {
        if (level < 70) {
            textView.setTextColor(resources.getColor(R.color.blue))
        } else if (level <= 100 && level >= 70) {
            textView.setTextColor(resources.getColor(R.color.green))
        } else if (level <= 125 && level > 100) {
            textView.setTextColor(resources.getColor(R.color.yellow))
        } else if (level > 125) {
            textView.setTextColor(resources.getColor(R.color.red))
        }
    }

    private fun checkAfterMeal(textView: TextView, level: Double) {
        if (level < 70) {
            textView.setTextColor(resources.getColor(R.color.blue))
        } else if (level <= 140 && level >= 70) {
            textView.setTextColor(resources.getColor(R.color.green))
        } else if (level <= 200 && level > 140) {
            textView.setTextColor(resources.getColor(R.color.yellow))
        } else if (level > 200) {
            textView.setTextColor(resources.getColor(R.color.red))
        }
    }


    private fun glucoseResults(
        date: String,
        title: String,
        number: Int,
        textViews: ArrayList<TextView>
    ) {
        val ColRef = db.collection("glucose").document(mAuth.getUid().toString()).collection(title)
        ColRef.whereEqualTo("date", date).get()
            .addOnCompleteListener { task: Task<QuerySnapshot?> ->
                if (task.isSuccessful) {
                    if (!task.result!!.isEmpty) {
                        val documentResults = task.result!!.toObjects(Measurement::class.java)
                        val glucoseResult = documentResults[0].glucose.toString()
                        val timeResult = documentResults[0].time.toString()
                        if (number > 0) {
                            checkAfterMeal(textViews[number], glucoseResult.toDouble())
                        } else {
                            checkOnEmpty(textViews[0], glucoseResult.toDouble())
                        }
                        textViews[number].text = glucoseResult
                        textViews[number + 4].text = timeResult

                    } else {
                        textViews[number].text = "-"
                        textViews[number].setTextColor(Color.BLACK)
                        textViews[number + 4].text = "-"
                    }

                } else {
                    Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
                }
            }
    }

    private fun pressureResults(date: String, number: Int, textViews: ArrayList<TextView>) {
        val ColRef =
            db.collection("pressure").document(mAuth.getUid().toString()).collection("pressure")
        ColRef.whereEqualTo("date", date).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (!task.result!!.isEmpty) {
                    val document = task.result?.toObjects(MultiMeasurement::class.java)
                    val length = document!![0].diastolicPressure!!.size
                    var diastSum = 0
                    var systSum = 0
                    for (j in 0..length - 1) {
                        val resultD = document[0].diastolicPressure!![j].toInt()
                        val resultS = document[0].systolicPressure!![j].toInt()
                        diastSum = diastSum + resultD
                        systSum = systSum + resultS
                    }
                    var systAvarage = systSum / length
                    var diastAvarage = diastSum / length
                    textViews[number].text =
                        systAvarage.toString() + " / " + diastAvarage.toString()
                } else {
                    textViews[number].text = "-"
                    textViews[number].setTextColor(Color.BLACK)
                }
            }

        }
    }

}