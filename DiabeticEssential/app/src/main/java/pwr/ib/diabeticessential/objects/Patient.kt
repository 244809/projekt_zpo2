package pwr.ib.diabeticessential.objects

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.Period.between
class Patient {
    var firstname: String? = null
    var lastname: String? = null
    var sex: String? = null

    var birth: String? = null
    var weight: Double? = null
    var height: Double? = null

    constructor(
        firstname: String?,
        lastname: String?,
        sex: String?,
        birth: String?
    ) {
        this.firstname = firstname
        this.lastname = lastname
        this.sex = sex
        this.birth = birth
        height = 0.0
        weight = 0.0
    }

    constructor() {}

    @RequiresApi(Build.VERSION_CODES.O)
    override fun toString(): String {
        var age = between(LocalDate.parse(birth),LocalDate.now()).years
        return """
            First name: $firstname
            Last name: $lastname
            Sex: $sex
            Birth date: $birth
            Age: $age
            """.trimIndent()
    }

    fun calculateBMI(): Double{
        var BMI = weight!! / Math.pow((height!! / 100), 2.0)
        return BMI;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun calculateAge(): Int{
        var age = between(LocalDate.parse(birth),LocalDate.now()).years
        return age;
    }

}
